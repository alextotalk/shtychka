package controllers

import (
	"html/template"
	"os"
	"time"
)

type Account struct {
	FirstName string
	LastName  string
}

type Purchase struct {
	Date          time.Time
	Description   string
	AmountInCents int
}

type Statement struct {
	FromDate  time.Time
	ToDate    time.Time
	Account   Account
	Purchases []Purchase
}

func Test(){
	t:=template.Must(template.New("base.tmpl").ParseFiles("base.tmpl"))
	err := t.Execute(os.Stdout, createMockStatement())
	if err != nil {
		panic(err)
	}
}

func createMockStatement() Statement {
	return Statement{
		FromDate: time.Date(2016, 1, 1, 0, 0, 0, 0, time.UTC),
		ToDate: time.Date(2016, 2, 1, 0, 0, 0, 0, time.UTC),
		Account: Account{
			FirstName: "John",
			LastName: "Dow",
		},
		Purchases: []Purchase {
			Purchase{
				Date: time.Date(2016, 1, 3, 0, 0, 0, 0, time.UTC),
				Description: "Shovel",
				AmountInCents: 2326,
			},
			Purchase{
				Date: time.Date(2016, 1, 8, 0, 0, 0, 0, time.UTC),
				Description: "Staple remover",
				AmountInCents: 5432,
			},
		},
	}
}