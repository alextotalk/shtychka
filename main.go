package main

import (
	"fmt"
	"net/http"
	"html/template"
)


func indexHandler(w http.ResponseWriter, r *http.Request)  {
	//server\
	t, err:=template.ParseFiles("templates/base.html", "templates/header.html", "templates/body.html", "templates/footer.html")
	if err!=nil{
		fmt.Fprintf(w, err.Error())
	}
	err=t.ExecuteTemplate(w, "base", nil)
	if err!=nil{
		fmt.Fprintf(w, err.Error())
	}
}

func main() {
	fmt.Println("Listening on port: 8080")
	http.HandleFunc("/", indexHandler)
	http.ListenAndServe(":8080", nil)
}


